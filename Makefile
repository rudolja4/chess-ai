
clean:
	rm -rf venv chess.egg-info

setup-dev: clean
	python3 -m venv venv
	./venv/bin/pip3 install -U -r requirements.txt
	./venv/bin/pip3 install -e .

export-libs:
	./venv/bin/pip3 freeze > requirements.txt

server:
	. ./venv/bin/activate && \
	python3 chess/run/app.py
