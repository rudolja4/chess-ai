from abc import ABC

from game.game.game_context import PlayerColor


class Player(ABC):
    def __init__(self, side: PlayerColor):
        self.side = side
