from abc import ABC, abstractmethod

from game.game.board import Board
from game.game.figure import Pawn, figure_class_map
from game.game.game_context import GameContext


class Impact(ABC):
    @abstractmethod
    def do(self, board: Board):
        pass

    @abstractmethod
    def is_valid(self, board: Board):
        pass


class MoveImpact(Impact):
    def __init__(self, from_square: tuple, to_square: tuple):
        self.from_square = from_square
        self.to_square = to_square

    def do(self, board: Board):
        assert board[self.to_square] is None, 'Move impact to occupied square!'
        board[self.to_square] = board[self.from_square]
        board[self.from_square] = None


class CaptureImpact(Impact):
    def __init__(self, square: tuple):
        self.square = square

    def do(self, board: Board):
        assert board[self.square] is not None, 'Capture impact on unoccupied square'
        board[self.square] = None


class UpgradeImpact(Impact):
    def __init__(self, square: tuple, target_figure_letter: str):
        self.square = square
        self.target_figure_letter = target_figure_letter

    def do(self, board: Board):
        assert board[self.square] is Pawn
        board[self.square] = figure_class_map[self.target_figure_letter.upper()]()


class Move:
    def __init__(self, impacts: list, time: int):
        self.impacts = impacts  # impacts must be in order: Capture, Move, Upgrade
        self.time = time  # time on the clock of the author of the stroke when the move was played in milliseconds

    def do(self, board: Board):
        pass

    def is_valid(self, board: Board, game_context: GameContext) -> bool:
        pass

    @staticmethod
    def from_string(move_code: str, board: Board) -> 'Move':
        pass
        '''
        TODO
        use figure_class_map
        add game_context param if needed
        '''

    def to_string(self, board: Board) -> str:
        pass
