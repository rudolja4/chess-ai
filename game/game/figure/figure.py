from abc import ABC, abstractmethod


class Figure(ABC):

    def __init__(self, side):
        self.side = side
        self.moved = False

    @abstractmethod
    def possible_moves(self, x, y, board):
        pass


