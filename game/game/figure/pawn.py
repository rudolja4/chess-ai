from game.game.board import Board
from game.game.figure.figure import Figure


class Pawn(Figure):

    def __init__(self, side):
        super().__init__(side)

    def possible_moves(self, x, y, board):
        yv = 1
        if self.side == 0:
            yv = -1
        move_vectors = [(0, yv)]
        if self.moved is False:
            move_vectors.append((0, yv*2))
        moves_to_show = []
        for i in move_vectors:
            if not Board.is_in_bounds(x + i[0], y + i[1]):
                continue
            elif board.is_empty(x + i[0], y + i[1]):
                moves_to_show.append((i[0]+x, y + i[1]))
            else:
                break
        move_vectors = [(1, yv), (-1, yv)]
        for i in move_vectors:
            if not Board.is_in_bounds(x + i[0], y + i[1]):
                continue
            elif board.is_empty(x + i[0], y + i[1]):
                continue
            elif board[x + i[0]][y + i[1]].side != self.side:
                moves_to_show.append((i[0]+x, y + i[1]))
            else:
                continue
        return moves_to_show
