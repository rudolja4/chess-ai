from game.game.board import Board
from game.game.figure.figure import Figure


class King(Figure):

    def __init__(self, side):
        super().__init__(side)

    def possible_moves(self, x, y, board):
        move_vectors = [(0, 1), (0, -1), (1, 0), (-1, 0), (1, 1), (1, -1), (-1, 1), (-1, -1)]
        moves_to_show = []
        for i in move_vectors:
            if Board.is_in_bounds(x + i[0], y + i[1]):
                if board.is_empty(x + i[0], y + i[1]):
                    moves_to_show.append((x + i[0], y + i[1]))
                elif board[x + i[0]][y + i[1]].side != self.side:
                    moves_to_show.append((x + i[0], y + i[1]))
                else:
                    continue
        return moves_to_show
