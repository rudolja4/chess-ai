from game.game.board import Board
from game.game.figure.figure import Figure


class Bishop(Figure):

    def __init__(self, side):
        super().__init__(side)

    def possible_moves(self, xx, yy, board):
        move_vectors = [(1, 1), (1, -1), (-1, 1), (-1, -1)]
        moves_to_show = []
        for i in move_vectors:
            x = xx
            y = yy
            while Board.is_in_bounds(x + i[0], y + i[1]):
                if board.is_empty(x + i[0], y + i[1]):
                    moves_to_show.append((x+i[0], y+i[1]))
                    x += i[0]
                    y += i[1]
                elif board[x + i[0]][y + i[1]].side != self.side:
                    moves_to_show.append((x + i[0], y + i[1]))
                    break
                else:
                    break
        return moves_to_show
