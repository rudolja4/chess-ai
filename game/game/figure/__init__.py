from game.game.figure.bishop import Bishop
from game.game.figure.knight import Knight
from game.game.figure.king import King
from game.game.figure.pawn import Pawn
from game.game.figure.queen import Queen
from game.game.figure.rook import Rook


figure_class_map = {
    'B': Bishop,
    'K': King,
    'Q': Queen,
    'R': Rook,
    'N': Knight,
    'P': Pawn
}
