from enum import Enum

from game.game.board import Board
from game.game.move.move import Move
from game.game.player import Player


class PlayerColor(Enum):
    WHITE = 'WHITE',
    BLACK = 'BLACK'


class PlayerContext:
    def __init__(self, queening_rank):
        self.queening_rank = queening_rank
        self.long_castle_flag = True  # long castle enabled, False once the Queen's Rook or King moves
        self.short_castle_flag = True  # short castle enabled, False once the King's Rook or King moves
        self.can_castle_long_flag = True  # flag for current round
        self.can_castle_short_flag = True  # flag for current round
        self.en_passant_flags = [False for _ in range(8)]  # enemy pawn on i-th file can be captured en passant
        self.check = False  # context owner is in check
        self.check_involved_squares = []  # squares which stop check by moving a figure on them (empty for double check) Necessary ?!
        
    def is_queening_square(self, pos):
        return pos[1] == self.queening_rank


class GameContext:
    def __init__(self, player_white: Player, player_black: Player):
        super().__init__()
        self.player_on_move = player_white
        self.player_contexts = {
            player_white: PlayerContext(0),
            player_black: PlayerContext(7)
        }

    def notify_player_moved(self, player: Player, board: Board, move: Move):
        played_context = self.player_contexts[player]
        other_context = [v for k, v in self.player_contexts.items() if k != player][0]

        # TODO nastavit a vyclearovat flagy

    def notify(self):
        pass
