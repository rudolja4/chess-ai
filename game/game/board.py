from game.game.figure import figure_class_map
from game.game.figure.bishop import Bishop
from game.game.figure.knight import Knight
from game.game.figure.king import King
from game.game.figure.pawn import Pawn
from game.game.figure.queen import Queen
from game.game.figure.rook import Rook


class Board:
    WIDTH = 8
    HEIGHT = 8

    def __init__(self):
        self.board = [[None for _ in range(8)] for _ in range(8)]

    def move_options(self, pos):
        return self[pos[0], pos[1]].possible_moves(pos[0], pos[1], self.board)

    def __getitem__(self, pos: tuple):
        return self.board[pos[0]][pos[1]]

    def __setitem__(self, pos: tuple, value):
        self.board[pos[0]][pos[1]] = value

    def is_empty(self, i, j):
        return self[i, j] is None

    @staticmethod
    def is_in_bounds(i, j):
        return 0 <= i < Board.WIDTH and 0 <= j < Board.HEIGHT

    @staticmethod
    def from_string(string: str) -> 'Board':
        board = Board()
        for i, line in enumerate(string.splitlines()):
            for j, char in enumerate(line):
                if char == ' ':
                    continue
                board[i, j] = figure_class_map[char.upper()](i, j, int(char.isupper()))
        return board

    @staticmethod
    def basic_setup() -> 'Board':
        board = Board()
        board[0, 0] = Rook(1)
        board[1, 0] = Knight(1)
        board[2, 0] = Bishop(1)
        board[3, 0] = King(1)
        board[4, 0] = Queen(1)
        board[5, 0] = Bishop(1)
        board[6, 0] = Knight(1)
        board[7, 0] = Rook(1)
        for i in range(8):
            board[i, 1] = Pawn(1)

        board[0, 7] = Rook(0)
        board[1, 7] = Knight(0)
        board[2, 7] = Bishop(0)
        board[3, 7] = King(0)
        board[4, 7] = Queen(0)
        board[5, 7] = Bishop(0)
        board[6, 7] = Knight(0)
        board[7, 7] = Rook(0)
        for i in range(8):
            board[i, 6] = Pawn(0)
        return board
